package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnoughIsEnough {

    //Given a list lst and a number N, create a new list that contains each number of lst at most N times without reordering.
    // For example if N = 2, and the input is [1,2,3,1,2,1,2,3], you take [1,2,3,1,2], drop the next [1,2] since this would
    // lead to 1 and 2 being in the result 3 times, and then take 3, which leads to [1,2,3,1,2,3].


    public static int[] deleteNth(int[] elements, int maxOccurrences) {
        final List<Integer> finalList = new ArrayList<>();
        Map<Integer, Integer> counterOfElement = new HashMap<>();
        for (Integer element : elements) {
            Integer count = counterOfElement.put(element, counterOfElement.getOrDefault(element, 0) + 1);
            if (count == null || count < maxOccurrences) {
                finalList.add(element);
            }
        }
        return finalList.stream().mapToInt(i -> i).toArray();
    }
}
