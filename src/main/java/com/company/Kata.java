package com.company;

public class Kata {
    //Simple, given a string of words, return the length of the shortest word(s).
    // String will never be empty and you do not need to account for different data types.

    public static int findShort(String s) {
        String[] stringArray = s.split(" ");
        int min = stringArray[0].length();
        for (String string : stringArray) {
            if (string.length() < min) {
                min = string.length();
            }
        }
        return min;
    }
}
