package com.company;

public class Line {

    //The new "Avengers" movie has just been released! There are a lot of people at the cinema box office standing in a huge line. Each of them has a single 100, 50 or 25 dollars bill. An "Avengers" ticket costs 25 dollars.
    //
    //Vasya is currently working as a clerk. He wants to sell a ticket to every single person in this line.
    //
    //Can Vasya sell a ticket to each person and give the change if he initially has no money and sells the tickets strictly in the order people follow in the line?
    //
    //Return YES, if Vasya can sell a ticket to each person and give the change with the bills he has at hand at that moment. Otherwise return NO.

    public static String Tickets(int[] peopleInLine) {
        int billon25 = 0;
        int billon50 = 0;

        for (int people : peopleInLine) {
            if (people == 25) {
                billon25++;
            }
            if (people == 50) {
                if (billon25 == 0) {
                    return "NO";
                }
                billon25--;
                billon50++;
            } else if (people == 100) {
                if(billon50<0 || billon25<3){
                    return "NO";
                }
                if(billon50>0){
                    billon50--;
                    billon25--;
                }else {
                    billon25-=3;
                }
            }
        }
        return "YES";
    }
}
