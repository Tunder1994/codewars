package com.company;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class LineTest {

    @Test
    public void tickets() {
        assertEquals("YES", Line.Tickets(new int[]{25,25,25,100}));
        assertEquals("NO", Line.Tickets(new int[]{25,100}));
    }
}